<?php 
/**
* Model Class
*
* This class is the base model class
*
* @version 1.0
*/

abstract class Model {
  protected $_db;

  public function __construct() {
    $this->_db = AppConfig::getConnection();
  }

  /*
  * Method controls all reading and filtering functionality for all models
  */ 
  public function readAll($filter = false) {
    $sql_query = "select main.*   ";
    if($this->join) {
      $sql_query .= ", count({$this->join}.{$this->joinby[0]}) as number ";
    }
    $sql_query .= "from ( select {$this->select} from {$this->table} ";
    $sql_query .= "where 1=1 ";
    if($filter){
      foreach ($filter as $key => $value) {
        $sql_query .= "and {$key} = {$value} ";
      }
    }
    $sql_query .= ") main ";
    if($this->join) {
      $sql_query .= "left join {$this->join} on {$this->join}.{$this->joinby[0]} = main.payload ";
      if(isset($this->joinby[1])) {
        $sql_query .= "and {$this->join}.{$this->joinby[1]} = main.{$this->joinby[1]} ";
      }
      $sql_query .= "group by {$this->groupby} ";
    }
    $sql_query .= "order by {$this->orderby} ";
    try {
      $stmt   = $this->_db->query($sql_query);
      $data  = $stmt->fetchAll(PDO::FETCH_OBJ);
      $dbCon = null;
      return $this->pasreData($data);
    }
    catch(PDOException $e) {
      echo '{"error":{"text":'. $e->getMessage() .'}}';
    } 
  }
}