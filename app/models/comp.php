<?php 
/**
* User Model Class
*
* This class models the user schema
*
* @version 1.0
*/

class Comp extends Model {
  protected $table = "comp";
  protected $select = "id as payload, comp_name as text";
  protected $join = "events";
  protected $joinby = ["cid"];
  protected $groupby = "events.cid";
  protected $orderby = "text ASC";
  public function pasreData($data){
    return $data;
  }
}