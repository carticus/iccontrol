<?php 
/**
* User Model Class
*
* This class models the user schema
*
* @version 1.0
*/

class Events extends Model {
  protected $table = "events";
  protected $select = "event_id as payload, event_name as text, cid";
  protected $join = "results";
  protected $joinby = ["event_id","cid"];
  protected $groupby = "results.event_id, results.cid";
  protected $orderby = "text ASC, payload DESC";
  public function pasreData($data){
    return $data;
  }
}