<?php 
/**
* User Model Class
*
* This class models the user schema
*
* @version 1.0
*/

class Results extends Model {
  protected $table = "results";
  protected $select = "rank, concat(firstname,' ',lastname) as name , clubname, resultms";
  protected $join = false;
  protected $orderby = " resultms ASC";
  public function pasreData($data){
    foreach ($data as $k => $row) {
      foreach ($row as $param => $value) {
        $content = new stdClass();
        if($param =='rank'){  
          $content->content = $k+1;
        }else if($param =='resultms'){
          $content->content = $this->convertTime($value);
        }else{
          $content->content = $value;
        }
        $row->$param = $content;
      };
    }
    return $data;
  }
  function convertTime($millis) {
    $ms = $millis%1000;
    $millis = $millis-$ms;
    $secs = ($millis/1000)%60;
    $millis = $millis-($secs*1000);
    $mins = $millis/(1000*60);
    return $mins.':'.$secs.'.'.$ms;
  }
}