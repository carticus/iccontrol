<?php 
/* Route to app page */
Router\Helper::map("pages", array(
	"(/|/home)/?"	=> array("get" => "home")
));

/* Route to API */
Router\Helper::map("api", array(
  "(/|/api)/?" => array("get" => "api"),
  "/api/all/:model?" => array("post" => "all"),
));

?>