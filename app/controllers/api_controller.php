<?php
/**
* API Controller
*
* This class is responsible to handle REST calls from the frontend
*
* @version 1.0
*/
class ApiController extends BaseController {

  /**
   * Root Route
   *
   **/
  protected function _api() {
    //echo "Api Root";
     $t = ["event_id"];
     var_dump($t);

  }
  
  function convertTime($millis) {
    $ms = $millis%1000;
    $millis = $millis-$ms;
    $secs = ($millis/1000)%60;
    $millis = $millis-($secs*1000);
    $mins = $millis/(1000*60);
    return ($mins.':'.$secs.'.'.$ms);
  }

  /**
   * Single gateway for all read requests
   *
   * @var JSON response
   **/
  public function _all() {
    $model = $this->params['model'];
    if(!$model){ return false; }
    $user = $this->loadModel($model);
    $response = Array();
    $reqbody = json_decode($this->request->getBody());
    $filter = isset($reqbody->data)?$reqbody->data:false;
    $response['data'] = $user->readAll($filter);
    echo json_encode($response); 
  }
}
