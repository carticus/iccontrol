<?php
/**
* Pages Controller
*
* This class is responsible rendering front End
*
* @version 1.0
*/
class PagesController extends BaseController {

	/**
	* Renders Home Page
	*/
	protected function _home() {
		$this->page_title = 'Home';
		$this->render("pages/index");
	}
}