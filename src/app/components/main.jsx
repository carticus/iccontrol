let React = require('react');
let mui = require('material-ui');
let Reflux = require('reflux');
let DataAction = require('../actions/dataAction');
let DataStore = require('../stores/dataStore');
let RaisedButton = mui.RaisedButton;
let LinearProgress = mui.LinearProgress;
let Table = mui.Table;
let Menu = mui.Menu;
let Dialog = mui.Dialog
let ThemeManager = new mui.Styles.ThemeManager();
let Colors = mui.Styles.Colors;

let Main = React.createClass({
  mixins: [Reflux.connect(DataStore,"data")],
  getInitialState() {
    return {
      fixedHeader: true,
      stripedRows: true,
      showRowHover: true,
      selectable: true,
      multiSelectable: true,
      canSelectAll: true,
      headerCols : {
        rank: {
          content: 'Placement',
          tooltip: 'the result'
        },
        name: {
          content: 'Name',
          tooltip: 'Competitors Name'
        },
        clubname: {
          content: 'Club Name',
          tooltip: 'The ClubName'
        },
        resultms: {
          content: 'Result',
          tooltip: 'The ClubName'
        }
      },
      colOrder : ['rank', 'name', 'clubname','resultms'],
      progressStyle : {
        display: 'none'
      },
      dialogStyle : {
        overflow: 'auto',
        maxHeight: '300px'
      },
      tableStyle : {
        display: 'none',
        height: '600px',
      },
      buttonStyle : {
        textAlign: 'center',
        display: 'block',
        paddingTop: '200px'
      }
    }
  },
  componentDidMount(){
    DataAction.readComp();
  },
  componentWillReceiveProps(nextProps) {
    this.setState({
      menuItems: nextProps.menuItems
    });
  },
  childContextTypes: {
    muiTheme: React.PropTypes.object
  },
  getChildContext() {
    return {
      muiTheme: ThemeManager.getCurrentTheme()
    };
  },
  componentWillMount() {
    ThemeManager.setPalette({
      accent1Color: Colors.deepOrange500
    });
  },
  render() {
    let compDialog,eventDialog,tableContainer;
    let progess = <LinearProgress style={this.state.progressStyle} ref="compProgress" mode="indeterminate"  />;
    if(this.state.data.compMenuItems.length>0){
      compDialog = <Dialog 
                    title="1. Select Competition"
                    ref="compDialog">
                    {progess}
                    <div style={this.state.dialogStyle} >
                      <Menu menuItems={this.state.data.compMenuItems} autoWidth={false} onItemTap={this._handleComp} />
                    </div>
                  </Dialog>;
    }
    if(this.state.data.eventMenuItems.length>0){
      eventDialog = <Dialog 
                      title="2. Select Event"
                      ref="eventDialog">
                      {progess}
                      <div style={this.state.dialogStyle} >
                      <Menu menuItems={this.state.data.eventMenuItems} autoWidth={false} onItemTap={this._handleEvent}/> 
                      </div>
                    </Dialog>;
    } 
    if(this.state.data.resultTableItems.length>0){
      tableContainer = <Table
                      headerColumns={this.state.headerCols}
                      columnOrder={this.state.colOrder}
                      rowData={this.state.data.resultTableItems}
                      height={this.state.tableStyle.height}
                      fixedHeader={this.state.fixedHeader}
                      fixedFooter={this.state.fixedFooter}
                      stripedRows={this.state.stripedRows}
                      showRowHover={this.state.showRowHover}
                      selectable={this.state.selectable}
                      multiSelectable={this.state.multiSelectable}
                      canSelectAll={this.state.canSelectAll} />;
    }
    let stepOne = <div style={this.state.buttonStyle}> 
                    <h1>IC Control - Events Assignment</h1>
                    <RaisedButton label="Start" primary={true} onTouchTap={this._handleStart} />
                  </div>;
    let stepTwo = <div>
                    {compDialog}
                  </div>;
    let stepThree = <div>
                    {eventDialog}
                  </div>;
    let stepFour = <div style={this.state.tableStyle}> 
                    {tableContainer} 
                  </div>;
    return (
      <div >
        {stepOne}    
        {stepTwo}   
        {stepThree}    
        {stepFour}
      </div>
    );
  },
  _handleStart() {
    this.refs.compDialog.show();
  },
  _handleComp(e, selectedIndex, menuItem) {
    if(menuItem.number>0){
      this.setState({'progressStyle':{ 'display' : 'block'}});
      DataAction.readEvents(menuItem.payload,this._completeComp);  
    }
  },
  _completeComp(){
    this.setState({'progressStyle':{ 'display' : 'none'}});
    this.refs.compDialog.dismiss();
    this.refs.eventDialog.show(); 
  },
  _handleEvent(e, selectedIndex, menuItem) {
    if(menuItem.number>0){
      this.setState({'progressStyle':{ 'display' : 'block'}});
      DataAction.readResults(menuItem.payload,this._completeEvent);
    }
  },
  _completeEvent(){
    this.setState({'progressStyle':{ 'display' : 'none'}});
    this.setState({'buttonStyle':{ 'paddingTop' : '0px', 'textAlign':'center'}});
    this.setState({'tableStyle':{ 'display' : 'block'}});
    this.refs.eventDialog.dismiss();
  },
});
module.exports = Main;
