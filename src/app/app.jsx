/* 
Author: Martin McCarthy
Bootstrap Loader for frontend 
*/
(function () {
  let React = require('react/addons'); // React lib
  let injectTapEventPlugin = require('react-tap-event-plugin');
  let Main = require('./components/main.jsx'); // Our main react component
  let Api = require('./services/api'); // api for connecting to our backend
  window.React = React; // attach React to window namespace
  injectTapEventPlugin(); // for mobile tap events
  React.render(<Main/>, document.body); // Render our main compnent to the dom
})();
