var Reflux = require('reflux'),
    dataAction = require('../actions/dataAction');

var dataStore = Reflux.createStore({
  listenables: [dataAction],
  data: {
    //api : 'http://54.170.210.231/index.php/api',
    api : 'http://ic.dev/api', 
    compMenuItems: [],
    eventMenuItems: [],
    resultTableItems: [],
    filter: {}
  },
  getInitialState() {
    return this.data
  },
  onReadComp(callback) {
    let object = {};
    return Api.call(this.data.api, 'all/comp', object, (response) => {
      this.data.compMenuItems = response.data || [];
      this.update();
      if (callback) {
        callback();
      }
    });
  },
  onReadEvents(id,callback) {
    this.data.filter = {};
    this.data.filter.cid = id;
    return Api.call(this.data.api, 'all/events', this.data.filter, (response) => {
      this.data.eventMenuItems = response.data || [];
      this.update();
      if (callback) {
        callback();
      }
    });
  },
  onReadResults(id,callback) {
    this.data.filter.event_id = id;
    return Api.call(this.data.api, 'all/results', this.data.filter, (response) => {
      this.data.resultTableItems = response.data || [];
      this.update();
      if (callback) {
        callback();
      }
    });
  },
  update(){
    this.trigger(this.data);
  }
});

module.exports = dataStore;