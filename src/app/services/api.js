import SuperAgent from 'superagent';
const ACCESS_DENIED = 201;

class Api {
  constructor() {
  }
  call(object, method, data, callback) {
    SuperAgent.post(object+'/'+method)
      .set('Accept', 'application/json')
      .set('Content-Type', 'text/plain')
      .send(JSON.stringify({method, data}))
      .end((err, response) => {
        if (!response.body) {
          response.body = SuperAgent.parse['application/json'](response.text);
        }
        if (response.body.isError && response.body.errCode == ACCESS_DENIED) {
          console.log(response);
          return;
        }
        callback(response.body);
      });
  }
};

var api = new Api();
window.Api = api;
export default api;
