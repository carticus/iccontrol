var Reflux = require('reflux');

var dataAction = Reflux.createActions(
  [
    'readComp',
    'readEvents',
    'readResults'
  ]
);

module.exports = dataAction;